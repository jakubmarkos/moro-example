package cz.moro.error;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

// Inspired by https://www.toptal.com/java/spring-boot-rest-api-error-handling
@JsonTypeName(value = "apierror")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class ApiError {

    public HttpStatus status;
    public String message;
    public List<ApiValidationError> validationErrors;

    public ApiError(HttpStatus status) {
        this.status = status;
        this.message = "Unexpected error";
    }

    public ApiError(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public void addValidationErrors(List<FieldError> fieldErrors) {
        if (validationErrors == null) {
            validationErrors = new ArrayList<>();
        }
        fieldErrors.forEach(this::addValidationError);
    }

    private void addValidationError(FieldError fieldError) {
        ApiValidationError error = new ApiValidationError();
        error.field = fieldError.getField();
        error.message = fieldError.getDefaultMessage();
        validationErrors.add(error);
    }

}