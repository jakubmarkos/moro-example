import React from 'react';

class UserRow extends React.Component {

    constructor(props) {
       super(props);
        this.state = {
            isBeingEdited: false,
            firstName: props.user.firstName,
            lastName: props.user.lastName
        };

        this.startEdit = this.startEdit.bind(this);
        this.save = this.save.bind(this);
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
    }

    startEdit() {
        this.setState({isBeingEdited: true});
    }

    save() {
        this.setState({isBeingEdited: false});
        this.props.editUser(this.props.user.id, this.state.firstName, this.state.lastName);
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value});
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value});
    }

    render() {
        let actionsCell = <ActionsCell id={this.props.user.id} deleteUser={this.props.deleteUser}
                              startEdit={this.startEdit} save={this.save} isBeingEdited={this.state.isBeingEdited} />;

        if (this.state.isBeingEdited) {
            return (
                <tr>
                    <TableCell value={this.props.user.id} />
                    <TableInputCell value={this.state.firstName} handleChange={this.handleFirstNameChange} />
                    <TableInputCell value={this.state.lastName} handleChange={this.handleLastNameChange} />
                    {actionsCell}
                </tr>
            );
        } else {
            return (
                <tr>
                    <TableCell value={this.props.user.id} />
                    <TableCell value={this.props.user.firstName} />
                    <TableCell value={this.props.user.lastName} />
                    {actionsCell}
                </tr>
            );
        }
	}
}

function TableCell(props) {
	return (
		<td>{props.value}</td>
	);
}

function TableInputCell(props) {
    return (
        <td>
            <input type="text" className="form-control"
                      value={props.value} onChange={props.handleChange}/>
        </td>
    );
}

function ActionsCell(props) {
    if (props.isBeingEdited) {
        return (
            <td className="text-center">
                <button type="button" className="btn btn-primary btn-sm" onClick={() => {props.save()}} >
                     Save
                 </button>
            </td>
        );
    } else {
        return (
            <td className="text-center">
                <button type="button" className="btn btn-primary btn-sm" onClick={() => {props.startEdit()}} >
                     Edit
                 </button>
                <button type="button" className="btn btn-danger btn-sm" onClick={() => {props.deleteUser(props.id)}} >
                  Delete
                </button>
            </td>
        );
    }
}

export default UserRow;