import React from 'react';
import UserTable from './UserTable';
import AddUserForm from './AddUserForm';
import ErrorPanel from './ErrorPanel';
import superagent from 'superagent';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {users: [], error: null};
		this.deleteUser = this.deleteUser.bind(this);
		this.addUser = this.addUser.bind(this);
		this.editUser = this.editUser.bind(this);
	}

	componentDidMount() {
		superagent
			.get('/api/users/')
            .then(res => {
                res.body.push({id: 100, firstName: "nonexistent", lastName: "nonexistent"})
                this.setState({
                    users: res.body
                });
            })
            .catch(err => {
                this.handleError(err);
            });
	}

	render() {
		return (
			<div className="container">
			    <ErrorPanel error={this.state.error} />
				<UserTable users={this.state.users} deleteUser={this.deleteUser} editUser={this.editUser} />
				<AddUserForm addUser={this.addUser} />
			</div>
		);
	}

	deleteUser(userId) {
		superagent
			.delete('/api/secured/users/'.concat(userId))
			.auth('brad', 'bradPass')
			.set('Content-Type', 'application/json')
			.then(res => {
				this.setState((state, props) => ({
					users: state.users.filter(user => user.id !== userId)
				}));
				this.clearError();
			})
			.catch(err => {
				this.handleError(err);
			});
	}

	addUser(firstName, lastName) {
		superagent
			.post('/api/users/')
			.send({firstName: firstName, lastName: lastName})
			.set('Content-Type', 'application/json')
			.then(res => {
                this.setState((state, props) => {
                    let newState = JSON.parse(JSON.stringify(state.users));
                    newState.push(res.body);
                    return {users: newState};
                });
                this.clearError();
            })
            .catch(err => {
                this.handleError(err);
            });
	}

    editUser(id, firstName, lastName) {
		superagent
			.put('/api/users/'.concat(id))
			.send({ firstName: firstName, lastName: lastName })
			.set('Content-Type', 'application/json')
			.then(res => {
                this.setState((state, props) => {
                    let newState = JSON.parse(JSON.stringify(state.users));
                    let i = newState.findIndex(user => user.id === id);
                    newState[i] = {id: id, firstName: res.body.firstName, lastName: res.body.lastName};
                    return {users: newState};
                });
                this.clearError();
            })
            .catch(err => {
                this.handleError(err);
            });
    }

    handleError(err) {
        let responseBody = err.response.body;
        if (responseBody != null && responseBody.hasOwnProperty("apierror")) {
            let apierror = err.response.body.apierror;
            this.setState({error: {message: apierror.message, validationErrors: apierror.validationErrors}});
        } else {
            if (responseBody != null && responseBody.hasOwnProperty("message")) {
				this.setState({error: {message: responseBody.message}})
			} else {
				this.setState({error: {message: "Unexpected error."}})
			}
        }
    }

    clearError() {
        this.setState({error: null});
    }
}

export default App;
