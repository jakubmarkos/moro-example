# moro-example

How to install:

1. mvn package
2. npm install

How to run:

1. start a PostgreSQL server, with a 'moro_example' database, and with 'postgres' user with password 'pass' (or change to custom values in application.properties)
2. java -jar target/moro-example-0.1.0.jar
3. npm start
