import React from 'react';

class AddUserForm extends React.Component {
    constructor(props) {
       super(props);
        this.state = {
            firstName: '',
            lastName: ''
        };

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value});
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value});
    }

    handleSubmit(event) {
        this.props.addUser(this.state.firstName, this.state.lastName);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor="inputFirstName">First name</label>
                      <input type="text" className="form-control" id="inputFirstName"
                          value={this.state.firstName} onChange={this.handleFirstNameChange}/>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="inputLastName">Last name</label>
                      <input type="text" className="form-control" id="inputLastName"
                          value={this.state.lastName} onChange={this.handleLastNameChange} />
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <button type="submit" className="btn btn-primary">Add new user</button>
                    </div>
                </div>
            </form>
        );
    }
}

export default AddUserForm;