import React from 'react';
import PropTypes from 'prop-types';
import UserRow from './UserRow';

class UserTable extends React.Component {
	render() {
		if (this.props.users === null || this.props.users.length === 0) {
			return (
			    <h1>No users</h1>
			);
		} else {
			return (
				<table className="table table-sm">
					<TableHead />
					<TableBody users={this.props.users} deleteUser={this.props.deleteUser} editUser={this.props.editUser} />
				</table>
			);
		}
	}
}

UserTable.propTypes = {
	users: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.number,
		firstName: PropTypes.string,
		lastName: PropTypes.string,
	}))
};

function TableHead(props) {
	return (
		<thead className="thead-dark">
			<tr>
				<th className="col-md-1">ID</th>
				<th className="col-md-2">First name</th>
				<th className="col-md-2">Last name</th>
				<th className="col-md-2"></th>
			</tr>
		</thead>
	);
}

function TableBody(props) {
	const usersRows = props.users.map((user) =>
		<UserRow key={user.id} user={user} deleteUser={props.deleteUser} editUser={props.editUser} />
	);
	return (
		<tbody>
			{usersRows}
		</tbody>
	);
}

export default UserTable;
