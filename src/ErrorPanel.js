import React from 'react';

class ErrorPanel extends React.Component {

	render() {
        if (this.props.error != null) {
            let validationErrors;
            if (this.props.error.validationErrors != null) {
                validationErrors = this.props.error.validationErrors.map((validationError) =>
                    <div>
                        Field {validationError.field}: {validationError.message}
                        <br />
                    </div>
                );
            }
            return (
                <div className="alert alert-danger" role="alert">
                  {this.props.error.message}
                  <br />
                  {validationErrors}
                </div>
            );
        } else {
            return (null);
        }
    }
}

export default ErrorPanel;