package cz.moro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import cz.moro.model.User;
import cz.moro.repository.UserRepository;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository repository;

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(new User("Brad", "Pitt", "brad", passwordEncoder.encode("bradPass")));
        this.repository.save(new User("Angelina", "Jolie", "angelina", passwordEncoder.encode("angelinaPass")));
        this.repository.save(new User("Steven", "Spielberg", "steven", passwordEncoder.encode("stevenPass")));
        this.repository.save(new User("Christopher", "Nolan", "chris", passwordEncoder.encode("chrisPass")));
        this.repository.save(new User("Bruce", "Wayne", "bruce", passwordEncoder.encode("brucePass")));
        this.repository.save(new User("Arthur", "Fleck", "arthur", passwordEncoder.encode("arthurPass")));
    }
}