package cz.moro.service;

import cz.moro.model.User;

import java.util.List;

public interface UserService {

    User getUser(Long id);

    List<User> getUsers();

    User createUser(User user);

    User updateUser(Long id, User user);

    void deleteUser(Long id);

}
